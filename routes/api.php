<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/cbr/new', 'CurrencyController@getLastData');
Route::get('/cbr/report', 'CurrencyController@getReport');
Route::get('/cbr/graph', 'CurrencyController@getGraph');
Route::get('/cbr/graph/one', 'CurrencyController@getGraphOne');
