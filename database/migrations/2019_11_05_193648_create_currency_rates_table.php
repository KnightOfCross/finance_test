<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCurrencyRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currency_rates', static function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('currency_id')->unsigned()->nullable(false);
            $table->integer('country_number_code')->nullable(false);
            $table->string('country_string_code')->nullable(false);
            $table->integer('nominal')->nullable(false);
            $table->float('value')->nullable(false);
            $table->date('date')->nullable(false);
        });

        Schema::table('currency_rates', static function (Blueprint $table) {
            $table->foreign('currency_id')->references('id')->on('currencies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('default');
    }
}
