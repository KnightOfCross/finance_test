<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <title>Laravel</title>
</head>
<body>

<div class="container">
    <div class="row">
        <button id="currency_getter" class="btn btn-info">Последние данные</button>
    </div>

    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('table') }}" method="GET">
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <label for="filter_name">Фильтрация по названию</label>
                        <input type="text" name="filter_name" id="filter_name" class="form-control" value="{{session('name')}}"/>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <label for="from">С</label>
                        <input type="text" id="from" name="from" class="form-control" value="{{session('from')}}">
                        <label for="to">По</label>
                        <input type="text" id="to" name="to" class="form-control" value="{{session('to')}}">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Поиск</button>
                <button class="btn btn-info" id="btn_report">Выгрузить JSON отчет</button>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-sm">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col"><a href="{{ Request::fullUrlWithQuery(['sort' => 'id']) }}">ID</a></th>
                    <th scope="col"><a href="{{ Request::fullUrlWithQuery(['sort' => 'country_number_code']) }}">Кодовый номер страны</a></th>
                    <th scope="col"><a href="{{ Request::fullUrlWithQuery(['sort' => 'country_string_code']) }}">Код страны</a></th>
                    <th scope="col"><a href="{{ Request::fullUrlWithQuery(['sort' => 'nominal']) }}">Номинал</a></th>
                    <th scope="col"><a href="{{ Request::fullUrlWithQuery(['sort' => 'name']) }}">Название</a></th>
                    <th scope="col"><a href="{{ Request::fullUrlWithQuery(['sort' => 'value']) }}">Число</a></th>
                    <th scope="col"><a href="{{ Request::fullUrlWithQuery(['sort' => 'date']) }}">Дата</a></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $item)
                    <tr>
                        <td>{{$item->id}}</td>
                        <td>{{$item->country_number_code}}</td>
                        <td>{{$item->country_string_code}}</td>
                        <td>{{$item->nominal}}</td>
                        <td>{{$item->currency()->first()->name}}</td>
                        <td>{{$item->value}}</td>
                        <td>{{$item->date}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <?= $data->appends(request()->query())->render() ?>
    <div class="row">
        <canvas id="myChart" width="400" height="300"></canvas>
    </div>
</div>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.2/dist/Chart.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/hammerjs@2.0.8"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-zoom@0.7.4"></script>
<script src="{{ asset('js/app.js') }}" type="text/js"></script>
<script>
    $(function () {

        function getGraphData() {
            return $.ajax({
                type: "GET",
                url: "/api/cbr/graph",
                data: {
                    to: $("#to").val(),
                    from: $("#from").val()
                },
                success: function (result) {
                    return result;
                },
                error: function (result) {
                }
            });
        }

        $.when(getGraphData()).done(function (data) {
            let ctx = document.getElementById('myChart');

            let labels = data['dates'];
            let datasets = [];

            jQuery.each(data['values'], function (nameCurrency, currencyValues) {
                let obj = {
                    label: nameCurrency,
                    data: currencyValues,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                };
                datasets.push(obj);
            });

            let myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: labels,
                    datasets: datasets
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                    pan: {
                        enabled: true,
                        mode: 'x',
                    },
                    zoom: {
                        enabled: true,
                        mode: 'x',
                    },
                    responsive: true
                }
            });
        });


        let to = $("#to").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 3
        })
            .on("change", function () {
                from.datepicker("option", "maxDate", getDate(this));
            });

        $("#currency_getter").click(function (e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "/api/cbr/new/",
                data: {
                    id: $(this).val(), // < note use of 'this' here
                    access_token: $("#access_token").val()
                },
                success: function (result) {
                    alert(result.status);
                },
                error: function (result) {
                    alert('error');
                }
            });
        });

        $("#btn_report").click(function (e) {
            e.preventDefault();
            window.location.href = '/download?to=' + $("#to").val() + '&from=' + $("#from").val();
        });

        let dateFormat = "dd.mm.yy",
            from = $("#from")
                .datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 3
                })
                .on("change", function () {
                    to.datepicker("option", "minDate", getDate(this));
                });

        function getDate(element) {
            let date;
            try {
                date = $.datepicker.parseDate(dateFormat, element.value);
            } catch (error) {
                date = null;
            }

            return date;
        }
    });
</script>
</body>
</html>
