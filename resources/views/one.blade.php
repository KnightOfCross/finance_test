@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Валюта: {{$name}}</h1>
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('one', ['id' => $id]) }}" method="GET">
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="demo">Введите временной промежуток</label>
                            <input type="text" id="demo" name="date_range" class="form-control"
                                   value="{{ session('date_range') }}">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Поиск</button>
                    <button class="btn btn-info" id="btn_report">Выгрузить JSON отчет</button>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col"><a href="{{ Request::fullUrlWithQuery(['sort' => 'id']) }}">ID</a></th>
                        <th scope="col"><a href="{{ Request::fullUrlWithQuery(['sort' => 'country_number_code']) }}">Кодовый
                                номер страны</a></th>
                        <th scope="col"><a href="{{ Request::fullUrlWithQuery(['sort' => 'country_string_code']) }}">Код
                                страны</a></th>
                        <th scope="col"><a href="{{ Request::fullUrlWithQuery(['sort' => 'nominal']) }}">Номинал</a>
                        </th>
                        <th scope="col"><a href="{{ Request::fullUrlWithQuery(['sort' => 'value']) }}">Число</a></th>
                        <th scope="col"><a href="{{ Request::fullUrlWithQuery(['sort' => 'date']) }}">Дата</a></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($data as $item)
                        <tr>
                            <td>{{$item->id}}</td>
                            <td>{{$item->country_number_code}}</td>
                            <td>{{$item->country_string_code}}</td>
                            <td>{{$item->nominal}}</td>
                            <td>{{$item->value}}</td>
                            <td>{{$item->date}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <?= $data->appends(request()->query())->render() ?>
        <div class="row">
            <canvas id="myChart" width="400" height="200"></canvas>
        </div>
        <div class="row">
            <div id="container" style="width:100%; height:400px;"></div>
        </div>
    </div>
@stop

@section('css')
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css"/>
@stop

@section('js')
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/date.js') }}"></script>
    {{--    <script src="{{ asset('js/graph.js') }}"></script>--}}
    <script>
        $(function () {

            $("#currency_getter").click(function (e) {
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "/api/cbr/new/",
                    data: {
                        id: $(this).val(),
                        access_token: $("#access_token").val()
                    },
                    success: function (result) {
                        alert(result.status);
                    },
                    error: function (result) {
                        alert('error');
                    }
                });
            });

            $("#btn_report").click(function (e) {
                e.preventDefault();
                window.location.href = '/download?date_range=' + $("#demo").val() + '&id=' + "{{$id}}";
            });

            $(window).on('load', function () {
                function getGraphData() {
                    return $.ajax({
                        type: "GET",
                        url: "/api/cbr/graph/one",
                        data: {
                            date_range: "{{session('date_range')}}",
                            id: "{{$id}}"
                        },
                        success: function (result) {
                            return result;
                        },
                        error: function (result) {
                        }
                    });
                }

                $.when(getGraphData()).done(function (data) {
                    console.log(data);
                    Highcharts.chart('container', {
                        chart: {
                            zoomType: 'x'
                        },
                        title: {
                            text: "{{$name}}"
                        },
                        subtitle: {
                            text: document.ontouchstart === undefined ?
                                'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
                        },
                        xAxis: {
                            type: 'datetime'
                        },
                        yAxis: {
                            title: {
                                text: 'Exchange rate'
                            }
                        },
                        legend: {
                            enabled: false
                        },
                        plotOptions: {
                            area: {
                                fillColor: {
                                    linearGradient: {
                                        x1: 0,
                                        y1: 0,
                                        x2: 0,
                                        y2: 1
                                    },
                                    stops: [
                                        [0, Highcharts.getOptions().colors[0]],
                                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                    ]
                                },
                                marker: {
                                    radius: 2
                                },
                                lineWidth: 1,
                                states: {
                                    hover: {
                                        lineWidth: 1
                                    }
                                },
                                threshold: null
                            }
                        },

                        series: [{
                            type: 'area',
                            name: "{{$name}}",
                            data: data.data
                        }]
                    });
                });
            });
        });

    </script>
@stop
