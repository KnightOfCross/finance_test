@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Таблица</h1>
@stop

@section('content')
    <div class="container">
        <div class="row">
            <button id="currency_getter" class="btn btn-info">Последние данные</button>
        </div>

        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('new_table') }}" method="GET">
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label for="filter_name">Фильтрация по названию</label>
                            <input type="text" name="filter_name" id="filter_name" class="form-control"
                                   value="{{session('name')}}"/>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="demo">Введите временной промежуток</label>
                            <input type="text" id="date_range" name="date_range" class="form-control"
                                   value="{{ session('date_range') }}">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Поиск</button>
                    <button class="btn btn-info" id="btn_report">Выгрузить JSON отчет</button>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-sm">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col"><a href="{{ Request::fullUrlWithQuery(['sort' => 'id']) }}">ID</a></th>
                        <th scope="col"><a href="{{ Request::fullUrlWithQuery(['sort' => 'country_number_code']) }}">Кодовый
                                номер страны</a></th>
                        <th scope="col"><a href="{{ Request::fullUrlWithQuery(['sort' => 'country_string_code']) }}">Код
                                страны</a></th>
                        <th scope="col"><a href="{{ Request::fullUrlWithQuery(['sort' => 'nominal']) }}">Номинал</a>
                        </th>
                        <th scope="col"><a href="{{ Request::fullUrlWithQuery(['sort' => 'name']) }}">Название</a></th>
                        <th scope="col"><a href="{{ Request::fullUrlWithQuery(['sort' => 'value']) }}">Число</a></th>
                        <th scope="col"><a href="{{ Request::fullUrlWithQuery(['sort' => 'date']) }}">Дата</a></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($data as $item)
                        <tr>
                            <td>{{$item->id}}</td>
                            <td>{{$item->country_number_code}}</td>
                            <td>{{$item->country_string_code}}</td>
                            <td>{{$item->nominal}}</td>
                            <td>
                                <a href="/one/{{$item->currency()->first()->valute_id}}">{{$item->currency()->first()->name}}</a>
                            </td>
                            <td>{{$item->value}}</td>
                            <td>{{$item->date}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <?= $data->appends(request()->query())->render() ?>
        <div class="row">
            <canvas id="myChart" width="400" height="300"></canvas>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@stop

@section('js')
    <script src="{{ asset('js/app.js') }}" type="text/js"></script>
    <script>
        $(function () {
            $("#currency_getter").click(function (e) {
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "/api/cbr/new/",
                    data: {
                        id: $(this).val(),
                        access_token: $("#access_token").val()
                    },
                    success: function (result) {
                        alert(result.status);
                    },
                    error: function (result) {
                        alert('error');
                    }
                });
            });

            $("#btn_report").click(function (e) {
                e.preventDefault();
                window.location.href = '/download?date_range=' + $("#demo").val();
            });
        });
    </script>
@stop
