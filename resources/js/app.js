require('./bootstrap');
require('jquery');
require('bootstrap-daterangepicker');
const moment = require('moment');
require('moment/locale/ru');
require('highcharts');

global.Highcharts = require('highcharts');


global.jquery = global.jQuery = global.$ = require('jquery');

$(function () {
    window.moment = moment;

    $(window).on('load', function () {
        $('#demo').daterangepicker({
            "showDropdowns": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "alwaysShowCalendars": true,
        }, function (start, end, label) {})
    });
});
