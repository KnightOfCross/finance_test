<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Domains\Models{
/**
 * App\Domains\CurrencyRate\Models\CurrencyRate
 *
 * @property int $id
 * @property string $valute_id
 * @property int $country_number_code
 * @property string $country_string_code
 * @property int $nominal
 * @property string $name
 * @property float $value
 * @property string $date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\CurrencyRate\Models\CurrencyRate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\CurrencyRate\Models\CurrencyRate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\CurrencyRate\Models\CurrencyRate query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\CurrencyRate\Models\CurrencyRate whereCountryNumberCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\CurrencyRate\Models\CurrencyRate whereCountryStringCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\CurrencyRate\Models\CurrencyRate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\CurrencyRate\Models\CurrencyRate whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\CurrencyRate\Models\CurrencyRate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\CurrencyRate\Models\CurrencyRate whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\CurrencyRate\Models\CurrencyRate whereNominal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\CurrencyRate\Models\CurrencyRate whereValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\CurrencyRate\Models\CurrencyRate whereValuteId($value)
 * @mixin \Eloquent
 */
	class Currency extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @mixin \Eloquent
 */
	class User extends \Eloquent {}
}

