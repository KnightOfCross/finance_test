## Инструкция
1. Скачать и распаковать архив
https://drive.google.com/file/d/1iJOWg6f5uXDmfpuD4MF0I0oEVtA7-Iae/view?usp=sharing

2. Выполнить в папке laradock:
    - docker-compose up -d

3. Выполнить:
    - winpty docker-compose exec workspace bash
    - composer install в папке с проектом /var/www/financial_service
    - artisan migrate
    - artisan cbr:meta - наполняет мета информацией о валютах
    - artisan cbr:collect - заполняет таблицу данными курсов с 1991 года
