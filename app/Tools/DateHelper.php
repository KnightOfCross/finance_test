<?php


namespace App\Tools;


use DateInterval;
use DatePeriod;
use DateTime;
use Exception;

/**
 * Class DateHelper
 *
 * @package App\Tools
 */
class DateHelper
{
    /**
     * @param string $firstDate
     *
     * @return DatePeriod
     * @throws Exception
     */
    public static function dateIterate($firstDate = '01/01/1991'): DatePeriod
    {
        $begin = new DateTime($firstDate);
        $currentDate = new DateTime();
        $interval = new DateInterval('P1D');

        return new DatePeriod($begin, $interval, $currentDate);
    }

    /**
     * Получает дату в сыром виде и форматирует в указанный формат
     *
     * @param null   $string
     * @param string $format
     *
     * @return string
     * @throws Exception
     */
    public static function standardize($string = null, $format = 'Y-m-d'): string
    {
        $dateTime = new DateTime($string);

        return self::reformat($dateTime, $format);
    }

    /**
     * @param DateTime $dateTime
     * @param string   $format
     *
     * @return string
     */
    public static function reformat(DateTime $dateTime, $format = 'Y-m-d'): string
    {
        return $dateTime->format('Y-m-d');
    }
}
