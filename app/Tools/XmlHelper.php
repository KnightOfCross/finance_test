<?php


namespace App\Tools;


use SimpleXMLElement;

/**
 * Class XmlHelper
 *
 * @package App\Tools
 */
class XmlHelper
{
    /**
     * @param $url
     *
     * @return SimpleXMLElement
     */
    public static function getXMLByURL($url): SimpleXMLElement
    {
        $context = stream_context_create(
            [
                'http' => [
                    'max_redirects' => 400,
                ],
            ]
        );

        $r = file_get_contents($url, false, $context);

        return simplexml_load_string($r);
    }
}
