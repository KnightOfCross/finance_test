<?php


namespace App\Tools;


/**
 * Class NumberHelper
 *
 * @package App\Tools
 */
class NumberHelper
{
    /**
     * Превращает строку в float, с условием что есть у строки есть не числовые элементы
     *
     * @param $num
     *
     * @return float
     */
    public static function tofloat($num): float
    {
        $dotPos = strrpos($num, '.');
        $commaPos = strrpos($num, ',');
        if ((($commaPos > $dotPos) && $commaPos)) {
            $sep = (($dotPos > $commaPos) && $dotPos)
                ? $dotPos
                :
                ($commaPos);
        } else {
            $sep = (($dotPos > $commaPos) && $dotPos)
                ? $dotPos
                :
                (false);
        }

        if (!$sep) {
            return (float)preg_replace('/\D/', '', $num);
        }

        return (float)(preg_replace('/\D/', '', substr($num, 0, $sep)).'.'.
            preg_replace('/\D/', '', substr($num, $sep + 1, strlen($num))));
    }
}
