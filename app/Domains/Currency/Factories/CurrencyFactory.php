<?php


namespace App\Domains\Currency\Factories;


use App\Domains\Currency\Models\Currency;
use App\Domains\Currency\Repositories\CurrencyRepository;

/**
 * Class CurrencyFactory
 *
 * @package App\Domains\Currency\Factories
 */
class CurrencyFactory
{
    /** @var CurrencyRepository */
    private $currencyRepository;

    /**
     * CurrencyFactory constructor.
     *
     * @param CurrencyRepository $currencyRepository
     */
    public function __construct(CurrencyRepository $currencyRepository)
    {
        $this->currencyRepository = $currencyRepository;
    }

    /**
     * @param $id
     * @param $name
     *
     * @return Currency
     */
    public function handle($id, $name): Currency
    {
        $model = new Currency();
        $model->name = $name;
        $model->valute_id = $id;

        return $this->currencyRepository->save($model);
    }
}
