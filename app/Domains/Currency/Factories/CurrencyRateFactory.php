<?php


namespace App\Domains\Currency\Factories;


use App\Domains\Currency\DTO\CurrencyRateCreateDTO;
use App\Domains\Currency\Models\CurrencyRate;
use App\Domains\Currency\Repositories\CurrencyRateRepository;

/**
 * Class CurrencyRateFactory
 *
 * @package App\Domains\CurrencyRate\Factories
 */
class CurrencyRateFactory
{
    /** @var CurrencyRateRepository */
    private $currencyRepository;

    /**
     * CurrencyRateFactory constructor.
     *
     * @param CurrencyRateRepository $currencyRepository
     */
    public function __construct(CurrencyRateRepository $currencyRepository)
    {
        $this->currencyRepository = $currencyRepository;
    }

    /**
     * @param CurrencyRateCreateDTO $createDTO
     */
    public function handle(CurrencyRateCreateDTO $createDTO): void
    {
        $model = new CurrencyRate();
        $model->date = $createDTO->getDate();
        $model->currency_id = $createDTO->getValuteId();
        $model->country_number_code = $createDTO->getCountryNumberCode();
        $model->country_string_code = $createDTO->getCountryStringCode();
        $model->nominal = $createDTO->getNominal();
        $model->value = $createDTO->getValue();

        $this->currencyRepository->save($model);
    }
}
