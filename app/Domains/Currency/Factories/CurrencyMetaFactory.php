<?php


namespace App\Domains\Currency\Factories;


use App\Domains\Currency\Models\CurrencyMeta;
use App\Domains\Currency\Repositories\CurrencyMetaRepository;
use DateTime;

/**
 * Class CurrencyMetaFactory
 *
 * @package App\Domains\CurrencyRate\Factories
 */
class CurrencyMetaFactory
{
    /** @var CurrencyMetaRepository */
    private $currencyMetaRepository;

    /**
     * CurrencyMetaFactory constructor.
     *
     * @param CurrencyMetaRepository $currencyMetaRepository
     */
    public function __construct(CurrencyMetaRepository $currencyMetaRepository)
    {
        $this->currencyMetaRepository = $currencyMetaRepository;
    }

    /**
     * @param DateTime $date
     *
     * @return CurrencyMeta
     */
    public function handle(DateTime $date): CurrencyMeta
    {
        $meta = new CurrencyMeta();
        $meta->date = $date;
        $meta->status = CurrencyMeta::STATUS_NEW;

        return $this->currencyMetaRepository->save($meta);
    }
}
