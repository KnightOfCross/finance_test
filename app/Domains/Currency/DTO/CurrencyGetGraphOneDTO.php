<?php


namespace App\Domains\Currency\DTO;


/**
 * Class CurrencyGetGraphOneDTO
 * @package App\Domains\Currency\DTO
 */
class CurrencyGetGraphOneDTO
{
    private $dateDTO;
    private $valute;


    /**
     * @return mixed
     */
    public function getValute()
    {
        return $this->valute;
    }

    /**
     * @param mixed $valute
     */
    public function setValute($valute): void
    {
        $this->valute = $valute;
    }

    /**
     * @return DateDTO
     */
    public function getDateDTO()
    {
        return $this->dateDTO;
    }

    /**
     * @param DateDTO $dateDTO
     */
    public function setDateDTO(DateDTO $dateDTO): void
    {
        $this->dateDTO = $dateDTO;
    }
}
