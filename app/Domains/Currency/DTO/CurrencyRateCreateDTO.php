<?php


namespace App\Domains\Currency\DTO;


/**
 * Class CurrencyRateCreateDTO
 *
 * @package App\Domains\CurrencyRate\DTO
 */
class CurrencyRateCreateDTO
{
    /** @var \DateTime */
    private $date;

    /** @var string */
    private $valuteId;

    /** @var int */
    private $countryNumberCode;

    /** @var string */
    private $countryStringCode;

    /** @var int */
    private $nominal;

    /** @var string */
    private $name;

    /** @var float */
    private $value;

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value): void
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getNominal()
    {
        return $this->nominal;
    }

    /**
     * @param mixed $nominal
     */
    public function setNominal($nominal): void
    {
        $this->nominal = $nominal;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date): void
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getCountryNumberCode()
    {
        return $this->countryNumberCode;
    }

    /**
     * @param mixed $countryNumberCode
     */
    public function setCountryNumberCode($countryNumberCode): void
    {
        $this->countryNumberCode = $countryNumberCode;
    }

    /**
     * @return mixed
     */
    public function getCountryStringCode()
    {
        return $this->countryStringCode;
    }

    /**
     * @param mixed $countryStringCode
     */
    public function setCountryStringCode($countryStringCode): void
    {
        $this->countryStringCode = $countryStringCode;
    }

    /**
     * @return mixed
     */
    public function getValuteId()
    {
        return $this->valuteId;
    }

    /**
     * @param mixed $valuteId
     */
    public function setValuteId($valuteId): void
    {
        $this->valuteId = $valuteId;
    }
}
