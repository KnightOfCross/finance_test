<?php


namespace App\Domains\Currency\DTO;


/**
 * Class DateDTO
 *
 * @package App\Domains\CurrencyRate\DTO
 */
class DateDTO
{
    private $from;
    private $to;

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param mixed $from
     */
    public function setFrom($from): void
    {
        $this->from = $from;
    }

    /**
     * @return mixed
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param mixed $to
     */
    public function setTo($to): void
    {
        $this->to = $to;
    }


}
