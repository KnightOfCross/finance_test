<?php


namespace App\Domains\Currency\Managers;


use App\Domains\Currency\Repositories\CurrencyMetaRepository;
use App\Domains\Currency\Models\CurrencyMeta;
use DateTime;

/**
 * Class CurrencyMetaManager
 *
 * @package App\Domains\CurrencyRate\Managers
 */
class CurrencyMetaManager
{
    /** @var CurrencyMetaRepository */
    private $currencyMetaRepository;

    /**
     * CurrencyMetaManager constructor.
     *
     * @param CurrencyMetaRepository $currencyMetaRepository
     */
    public function __construct(CurrencyMetaRepository $currencyMetaRepository)
    {
        $this->currencyMetaRepository = $currencyMetaRepository;
    }

    /**
     * @param CurrencyMeta $meta
     */
    public function metaComplete(CurrencyMeta $meta): void
    {
        $meta->status = CurrencyMeta::STATUS_COMPLETED;
        $this->currencyMetaRepository->save($meta);
    }

    /**
     * @param CurrencyMeta $meta
     */
    public function metaInProgress(CurrencyMeta $meta): void
    {
        $meta->status = CurrencyMeta::STATUS_IN_PROGRESS;
        $this->currencyMetaRepository->save($meta);
    }
}
