<?php


namespace App\Domains\Currency\Managers;


use App\Domains\Currency\DTO\CurrencyGetGraphOneDTO;
use App\Domains\Currency\DTO\CurrencyRateCreateDTO;
use App\Domains\Currency\Factories\CurrencyFactory;
use App\Domains\Currency\Factories\CurrencyRateFactory;
use App\Domains\Currency\Models\CurrencyRate;
use App\Domains\Currency\Repositories\CurrencyCriteriasRepository;
use App\Domains\Currency\Repositories\CurrencyRateCriteriasRepository;
use App\Domains\Currency\Repositories\CurrencyRateRepository;
use App\Domains\Currency\Repositories\CurrencyRepository;
use App\Tools\NumberHelper;
use DateTime;
use Exception;
use Illuminate\Support\Facades\Date;
use SimpleXMLElement;

/**
 * Class CurrencyRateManager
 *
 * @package App\Domains\CurrencyRate\Managers
 */
class CurrencyRateManager
{
    /** @var CurrencyRateFactory */
    private $currencyRateFactory;

    /** @var CurrencyRateRepository */
    private $currencyRateRepository;

    /** @var CurrencyRepository */
    private $currencyRepository;

    /** @var CurrencyFactory */
    private $currencyFactory;

    /** @var CurrencyRateCriteriasRepository */
    private $currencyRateCriteriasRepository;

    /** @var CurrencyCriteriasRepository */
    private $currencyCriteriasRepository;

    /**
     * CurrencyRateManager constructor.
     *
     * @param CurrencyRateFactory             $currencyRateFactory
     * @param CurrencyRateRepository          $currencyRateRepository
     * @param CurrencyRepository              $currencyRepository
     * @param CurrencyFactory                 $currencyFactory
     * @param CurrencyRateCriteriasRepository $currencyRateCriteriasRepository
     * @param CurrencyCriteriasRepository     $currencyCriteriasRepository
     */
    public function __construct(
        CurrencyRateFactory $currencyRateFactory,
        CurrencyRateRepository $currencyRateRepository,
        CurrencyRepository $currencyRepository,
        CurrencyFactory $currencyFactory,
        CurrencyRateCriteriasRepository $currencyRateCriteriasRepository,
        CurrencyCriteriasRepository $currencyCriteriasRepository
    ) {
        $this->currencyRateFactory             = $currencyRateFactory;
        $this->currencyRateRepository          = $currencyRateRepository;
        $this->currencyRepository              = $currencyRepository;
        $this->currencyFactory                 = $currencyFactory;
        $this->currencyRateCriteriasRepository = $currencyRateCriteriasRepository;
        $this->currencyCriteriasRepository     = $currencyCriteriasRepository;
    }

    /**
     * @param SimpleXMLElement $child
     * @param DateTime         $date
     */
    public function buildCurrency(SimpleXMLElement $child, DateTime $date): void
    {
        $createDTO = new CurrencyRateCreateDTO();
        $createDTO->setDate($date);
        $createDTO->setCountryNumberCode((string)$child->NumCode);
        $createDTO->setCountryStringCode((string)$child->CharCode);
        $createDTO->setNominal((int)$child->Nominal);
        $createDTO->setName((string)$child->Name);
        $createDTO->setValue(NumberHelper::tofloat($child->Value));

        $currency = $this->currencyRepository->findOneByValuteId((string)$child['ID']);
        if (!$currency) {
            $id   = (string)$child['ID'];
            $name = (string)$child->Name;

            $currency = $this->currencyFactory->handle($id, $name);
        }

        $createDTO->setValuteId($currency->id);

        $this->currencyRateFactory->handle($createDTO);
    }

    /**
     * Получить список названий валют
     *
     * @param $from
     * @param $to
     *
     * @return array
     */
    public function getRawNames($from, $to): array
    {
        $modelsNames = $this->currencyRateRepository->findCurrencyBetweenDate($from, $to, true);

        $names              = [];
        $currencyRepository = $this->currencyRepository;
        $modelsNames->each(static function ($modelName) use (&$names, $currencyRepository) {
            $currency = $currencyRepository->findOneById($modelName->currency_id);

            $names[] = $currency->name;
        });

        return $names;
    }

    /**
     * @param $from
     * @param $to
     *
     * @return array
     * @throws Exception
     */
    public function getRawDates($from, $to): array
    {
        $modelsDates = $this->currencyRateRepository->findDateBetweenDate($from, $to, true);

        $dates = [];
        $modelsDates->each(static function ($modelsDate) use (&$dates) {
            $dates[] = $modelsDate->date;
        });

        return $dates;
    }

    /**
     * @param CurrencyGetGraphOneDTO $DTO
     *
     * @return array
     * @throws Exception
     */
    public function getFormattedCurrency(CurrencyGetGraphOneDTO $DTO): array
    {
        $query = CurrencyRate::query();
        $this->currencyRateCriteriasRepository->betweenDateCriteria($query, $DTO->getDateDTO());
        $this->currencyRateCriteriasRepository->joinCurrencyCriteria($query);
        $this->currencyCriteriasRepository->findByValuteIdCriteria($query, $DTO->getValute());
        $currencyModels = $query->get();

        $dates = $this->getRawDates($DTO->getDateDTO()->getFrom(), $DTO->getDateDTO()->getTo());

        $name = $query->first()->name;

        $fullDatesWithNames = [];
        foreach ($dates as $date) {
            $fullDatesWithNames[$date][$name] = null;
        }

        foreach ($currencyModels as $model) {
            $fullDatesWithNames[$model->date][$model->currency()->first()->name] = $model->value;
        }

        $currencies = [];
        foreach ($fullDatesWithNames as $item) {
            foreach ($item as $key => $value) {
                if ($key === $name) {
                    $currencies[$name][] = $item[$name];
                }
            }
        }

        return $currencies;
    }


    /**
     * @param CurrencyGetGraphOneDTO $dto
     *
     * @return array
     */
    public function getGraphData(CurrencyGetGraphOneDTO $dto)
    {
        $query = CurrencyRate::query();
        $this->currencyRateCriteriasRepository->joinCurrencyCriteria($query);
        $this->currencyCriteriasRepository->findByValuteIdCriteria($query, $dto->getValute());
        $this->currencyRateCriteriasRepository->betweenDateCriteria($query, $dto->getDateDTO());
        $modelsDates = $query->get();

        $dates = [];

        $modelsDates->each(static function ($modelsDate) use (&$dates) {
            /** @var CurrencyRate $modelsDate */
            $dates[] = [
                Date::parse($modelsDate->date)->valueOf(),
                $modelsDate->value,
            ];
        });

        return $dates;
    }
}
