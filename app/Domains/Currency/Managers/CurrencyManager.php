<?php


namespace App\Domains\Currency\Managers;


use App\Domains\Currency\Factories\CurrencyFactory;
use App\Domains\Currency\Repositories\CurrencyRepository;
use SimpleXMLElement;

/**
 * Class CurrencyManager
 *
 * @package App\Domains\Currency\Managers
 */
class CurrencyManager
{
    /** @var CurrencyFactory */
    private $currencyFactory;

    /** @var CurrencyRepository */
    private $currencyRepository;

    /**
     * CurrencyManager constructor.
     *
     * @param CurrencyFactory    $currencyFactory
     * @param CurrencyRepository $currencyRepository
     */
    public function __construct(CurrencyFactory $currencyFactory, CurrencyRepository $currencyRepository)
    {
        $this->currencyFactory = $currencyFactory;
        $this->currencyRepository = $currencyRepository;
    }

    /**
     * @param SimpleXMLElement $child
     */
    public function buildCurrency(SimpleXMLElement $child): void
    {
        $id = (string)$child['ID'];
        $name = (string)$child->Name;

        $currency = $this->currencyRepository->findOneByValuteId($id);

        if (!$currency) {
            $this->currencyFactory->handle($id, $name);
        }
    }
}
