<?php


namespace App\Domains\Currency\Models;


use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Currency
 *
 * @package App\Domains\Currency\Models
 * @property int    $id
 * @property string $valute_id
 * @property string $name
 * @method static Builder|Currency newModelQuery()
 * @method static Builder|Currency newQuery()
 * @method static Builder|Currency query()
 * @method static Builder|Currency whereId($value)
 * @method static Builder|Currency whereName($value)
 * @method static Builder|Currency whereValuteId($value)
 * @mixin Eloquent
 */
class Currency extends Model
{
    protected $table = 'currencies';
    public $timestamps = false;

    /** @return string */
    public static function getTableName()
    {
        return 'currencies';
    }
}
