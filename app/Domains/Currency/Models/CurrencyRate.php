<?php


namespace App\Domains\Currency\Models;


use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class CurrencyRate
 *
 * @package App\Domains\Currency\Models
 * @property int    $id
 * @property int    $currency_id
 * @property int    $country_number_code
 * @property string $country_string_code
 * @property int    $nominal
 * @property float  $value
 * @property string $date
 * @method static Builder|CurrencyRate newModelQuery()
 * @method static Builder|CurrencyRate newQuery()
 * @method static Builder|CurrencyRate query()
 * @method static Builder|CurrencyRate whereCountryNumberCode($value)
 * @method static Builder|CurrencyRate whereCountryStringCode($value)
 * @method static Builder|CurrencyRate whereCurrencyId($value)
 * @method static Builder|CurrencyRate whereDate($value)
 * @method static Builder|CurrencyRate whereId($value)
 * @method static Builder|CurrencyRate whereNominal($value)
 * @method static Builder|CurrencyRate whereValue($value)
 * @mixin Eloquent
 */
class CurrencyRate extends Model
{
    protected $table = 'currency_rates';
    public $timestamps = false;

    /**
     * @return BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }
}
