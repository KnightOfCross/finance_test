<?php


namespace App\Domains\Currency\Models;


use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CurrencyMeta
 *
 * @package App\Domains\Currency\Models
 * @property int    $id
 * @property string $date
 * @property string $status
 * @method static Builder|CurrencyMeta newModelQuery()
 * @method static Builder|CurrencyMeta newQuery()
 * @method static Builder|CurrencyMeta query()
 * @method static Builder|CurrencyMeta whereDate($value)
 * @method static Builder|CurrencyMeta whereId($value)
 * @method static Builder|CurrencyMeta whereStatus($value)
 * @mixin Eloquent
 */
class CurrencyMeta extends Model
{
    protected $table = 'currencies_meta';
    public $timestamps = false;

    public const STATUS_COMPLETED = 'completed';
    public const STATUS_IN_PROGRESS = 'in_progress';
    public const STATUS_NEW = 'new';
}
