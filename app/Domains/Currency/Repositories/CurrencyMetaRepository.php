<?php


namespace App\Domains\Currency\Repositories;


use App\Domains\Currency\Models\CurrencyMeta;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CurrencyMetaRepository
 *
 * @package App\Domains\CurrencyRate\Repositories
 */
class CurrencyMetaRepository
{
    /**
     * @param CurrencyMeta $model
     *
     * @return CurrencyMeta
     */
    public function save(CurrencyMeta $model): CurrencyMeta
    {
        $model->save();
        $model->refresh();

        return $model;
    }

    /**
     * @param string $date
     *
     * @return Builder|Model|object|null|CurrencyMeta
     */
    public function findOneByDate(string $date)
    {
        return CurrencyMeta::where('date', $date)->first();
    }
}
