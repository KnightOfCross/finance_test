<?php


namespace App\Domains\Currency\Repositories;


use App\Domains\Currency\DTO\DateDTO;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CurrencyRateCriteriasRepository
 *
 * @package App\Domains\CurrencyRate\Repositories
 */
class CurrencyRateCriteriasRepository
{
    /**
     * @param Builder $query
     *
     * @return Builder
     */
    public function selectReportCriteria(Builder $query): Builder
    {
        return $query->select(['name', 'country_string_code', 'value', 'nominal', 'date']);
    }

    /**
     * @param Builder $query
     * @param DateDTO $dateDTO
     *
     * @return Builder
     */
    public function betweenDateCriteria(Builder $query, DateDTO $dateDTO): Builder
    {
        return $query->whereBetween('date', [$dateDTO->getFrom(), $dateDTO->getTo()]);
    }

    /**
     * @param Builder $query
     * @param string  $date
     *
     * @return Builder
     */
    public function findByDateCriteria(Builder $query, string $date)
    {
        return $query->where('date', $date);
    }

    /**
     * @param Builder $query
     *
     * @return Builder
     */
    public function joinCurrencyCriteria(Builder $query): Builder
    {
        return $query->join('currencies', 'currencies.id', '=', 'currency_rates.currency_id');
    }
}
