<?php


namespace App\Domains\Currency\Repositories;


use App\Domains\Currency\Models\Currency;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CurrencyCriteriasRepository
 * @package App\Domains\Currency\Repositories
 */
class CurrencyCriteriasRepository
{
    /**
     * @param Builder $query
     * @param         $valuteId
     *
     * @return Builder
     */
    public function findByValuteIdCriteria(Builder $query, $valuteId)
    {
        return $query->where(Currency::getTableName() . '.' . 'valute_id', $valuteId);
    }
}
