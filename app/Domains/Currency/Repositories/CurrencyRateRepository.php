<?php


namespace App\Domains\Currency\Repositories;


use App\Domains\Currency\Models\CurrencyRate;
use DateTime;
use Exception;
use Illuminate\Support\Collection;

/**
 * Class CurrencyRateRepository
 *
 * @package App\Domains\CurrencyRate\Repositories
 */
class CurrencyRateRepository
{
    /**
     * @param CurrencyRate $model
     */
    public function save(CurrencyRate $model): void
    {
        $model->save();
    }

    /**
     * @param string $date
     */
    public function deleteAllByDate(string $date): void
    {
        CurrencyRate::where('date', $date)->delete();
    }

    /**
     * @param      $from
     * @param      $to
     * @param bool $distinct
     *
     * @return Collection
     * @throws Exception
     */
    public function findDateBetweenDate($from, $to, $distinct = false): Collection
    {
        $query = CurrencyRate::whereBetween('date', [$from, $to]);

        if ($distinct) {
            $query->distinct();
        }

        return $query->get(['date']);
    }

    /**
     * @param      $from
     * @param      $to
     * @param bool $distinct
     *
     * @return Collection
     */
    public function findCurrencyBetweenDate($from, $to, $distinct = false): Collection
    {
        $query = CurrencyRate::whereBetween('date', [$from, $to]);

        if ($distinct) {
            $query->distinct();
        }

        return $query->get(['currency_id']);
    }

    /**
     * @param $from
     * @param $to
     *
     * @return Collection|CurrencyRate[]
     */
    public function findAllBetweenDate($from, $to): Collection
    {
        return CurrencyRate::whereBetween('date', [$from, $to])
            ->orderByDesc('id')
            ->get();
    }
}
