<?php


namespace App\Domains\Currency\Repositories;


use App\Domains\Currency\Models\Currency;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CurrencyRepository
 *
 * @package App\Domains\Currency\Repositories
 */
class CurrencyRepository
{
    /**
     * @param $id
     *
     * @return Currency|Builder|Model|object|null
     */
    public function findOneByValuteId(string $id)
    {
        return Currency::whereValuteId($id)->first();
    }

    /**
     * @param Currency $model
     *
     * @return Currency
     */
    public function save(Currency $model): Currency
    {
        $model->save();
        $model->refresh();

        return $model;
    }

    /**
     * @param int $id
     *
     * @return Currency|Builder|Model|object|null
     */
    public function findOneById(int $id)
    {
        return Currency::whereId($id)->first();
    }
}
