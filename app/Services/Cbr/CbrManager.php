<?php


namespace App\Services\Cbr;

/**
 * Class CbrManager
 *
 * @package App\Services\Cbr
 */
class CbrManager
{
    /***
     * @param string|null $date
     *
     * @return string
     */
    public function getUrl(string $date = null): string
    {
        $url = env('CBR_SOURCE');

        if ($date) {
            $url .= '?date_req='.$date;
        }

        return $url;
    }

    /**
     * d=0 Коды валют устанавливаемые ежедневно.
     * d=1 Коды валют устанавливаемые ежемесячно.
     *
     * @param bool $mode
     *
     * @return mixed|string
     */
    public function getUrlCurrencies(bool $mode)
    {
        $url = env('CBR_META_SOURCE');

        $url = $mode ? $url.'?d='.$mode : $url.'?d=0';

        return $url;
    }
}
