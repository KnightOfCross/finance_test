<?php

namespace App\Console\Commands;

use App\Domains\Currency\Factories\CurrencyMetaFactory;
use App\Domains\Currency\Managers\CurrencyRateManager;
use App\Domains\Currency\Managers\CurrencyMetaManager;
use App\Domains\Currency\Models\CurrencyMeta;
use App\Domains\Currency\Repositories\CurrencyMetaRepository;
use App\Domains\Currency\Repositories\CurrencyRateRepository;
use App\Services\Cbr\CbrManager;
use App\Tools\DateHelper;
use App\Tools\XmlHelper;
use DateTime;
use Exception;
use Illuminate\Console\Command;

/**
 * Class CbrDataCommand
 *
 * @package App\Console\Commands
 */
class CbrDataCommand extends Command
{
    /** @var string */
    protected $signature = 'cbr:collect';

    /** @var string */
    protected $description = 'Command description';

    /** @var CurrencyRateRepository */
    private $currencyRepository;

    /** @var CbrManager */
    private $cbrManager;

    /** @var CurrencyMetaRepository */
    private $currencyMetaRepository;

    /** @var CurrencyMetaFactory */
    private $currencyMetaFactory;

    /** @var CurrencyMetaManager */
    private $currencyMetaManager;

    /** @var CurrencyRateManager */
    private $currencyManager;

    /**
     * Create a new command instance.
     *
     * @param CurrencyRateRepository $currencyRepository
     * @param CbrManager             $cbrManager
     * @param CurrencyMetaRepository $currencyMetaRepository
     * @param CurrencyMetaFactory    $currencyMetaFactory
     * @param CurrencyMetaManager    $currencyMetaManager
     * @param CurrencyRateManager    $currencyManager
     */
    public function __construct(
        CurrencyRateRepository $currencyRepository,
        CbrManager $cbrManager,
        CurrencyMetaRepository $currencyMetaRepository,
        CurrencyMetaFactory $currencyMetaFactory,
        CurrencyMetaManager $currencyMetaManager,
        CurrencyRateManager $currencyManager
    ) {
        parent::__construct();
        $this->currencyRepository = $currencyRepository;
        $this->cbrManager = $cbrManager;
        $this->currencyMetaRepository = $currencyMetaRepository;
        $this->currencyMetaFactory = $currencyMetaFactory;
        $this->currencyMetaManager = $currencyMetaManager;
        $this->currencyManager = $currencyManager;
    }

    /**
     * Команда обращается на сайт CBR и вытаскивыает все данные по дням
     * С 1991 года по текущий день
     *
     * @return mixed
     * @throws Exception
     */
    public function handle()
    {
        $dateIterator = DateHelper::dateIterate();

        /** @var DateTime $date */
        foreach ($dateIterator as $date) {
            $meta = $this->currencyMetaRepository->findOneByDate(DateHelper::reformat($date));

            if (!$meta) {
                $meta = $this->currencyMetaFactory->handle($date);
            } else {
                if ($meta->status === CurrencyMeta::STATUS_COMPLETED) {
                    continue;
                }

                if ($meta->status === CurrencyMeta::STATUS_IN_PROGRESS) {
                    $meta->status = CurrencyMeta::STATUS_NEW;
                    $this->currencyMetaRepository->save($meta);
                    $this->currencyRepository->deleteAllByDate(DateHelper::reformat($date));
                }
            }

            $url = $this->cbrManager->getUrl($date->format('d/m/Y'));
            $xml = XmlHelper::getXMLByURL($url);

            if (trim((string)$xml) === 'Error in parameters') {
                $this->currencyMetaManager->metaComplete($meta);
                continue;
            }

            $this->currencyMetaManager->metaInProgress($meta);

            $dateXml = null;
            if (isset($xml['Date'])) {
                $dateXml = (new DateTime((string)$xml['Date']));
            } else {
                $dateXml = $date;
            }

            $metaExist = $this->currencyMetaRepository->findOneByDate(DateHelper::reformat($dateXml));
            if ($metaExist && $metaExist->status === CurrencyMeta::STATUS_COMPLETED) {
                $this->currencyMetaManager->metaComplete($meta);

                continue;
            }

            foreach ($xml->children() as $child) {
                $this->currencyManager->buildCurrency($child, $date);
            }

            $this->currencyMetaManager->metaComplete($meta);
        }
    }
}
