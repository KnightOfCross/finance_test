<?php

namespace App\Console\Commands;

use App\Domains\Currency\Managers\CurrencyManager;
use App\Services\Cbr\CbrManager;
use App\Tools\XmlHelper;
use Illuminate\Console\Command;

class CbrMetaCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cbr:meta';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /** @var CbrManager */
    private $cbrManager;

    /** @var CurrencyManager */
    private $currencyManager;

    /**
     * Create a new command instance.
     *
     * @param CbrManager      $cbrManager
     * @param CurrencyManager $currencyManager
     */
    public function __construct(CbrManager $cbrManager, CurrencyManager $currencyManager)
    {
        parent::__construct();
        $this->cbrManager = $cbrManager;
        $this->currencyManager = $currencyManager;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $url = $this->cbrManager->getUrlCurrencies(true);
        $xml = XmlHelper::getXMLByURL($url);

        foreach ($xml->children() as $child) {
            $this->currencyManager->buildCurrency($child);
        }


        $url = $this->cbrManager->getUrlCurrencies(false);
        $xml = XmlHelper::getXMLByURL($url);

        foreach ($xml->children() as $child) {
            $this->currencyManager->buildCurrency($child);
        }
    }
}
