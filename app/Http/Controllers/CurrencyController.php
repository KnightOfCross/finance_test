<?php

namespace App\Http\Controllers;

use App\Domains\Currency\DTO\CurrencyGetGraphOneDTO;
use App\Domains\Currency\DTO\DateDTO;
use App\Domains\Currency\Factories\CurrencyMetaFactory;
use App\Domains\Currency\Managers\CurrencyMetaManager;
use App\Domains\Currency\Managers\CurrencyRateManager;
use App\Domains\Currency\Models\CurrencyMeta;
use App\Domains\Currency\Models\CurrencyRate;
use App\Domains\Currency\Repositories\CurrencyCriteriasRepository;
use App\Domains\Currency\Repositories\CurrencyMetaRepository;
use App\Domains\Currency\Repositories\CurrencyRateCriteriasRepository;
use App\Domains\Currency\Repositories\CurrencyRateRepository;
use App\Domains\Currency\Repositories\CurrencyRepository;
use App\Services\Cbr\CbrManager;
use App\Tools\DateHelper;
use App\Tools\XmlHelper;
use DateTime;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\StreamedResponse;
use function request;

/**
 * Class CurrencyController
 *
 * @package App\Http\Controllers
 */
class CurrencyController extends Controller
{
    /** @var CurrencyMetaRepository */
    private $currencyMetaRepository;

    /** @var CurrencyMetaFactory */
    private $currencyMetaFactory;

    /** @var CurrencyRateRepository */
    private $currencyRateRepository;

    /** @var CbrManager */
    private $cbrManager;

    /** @var CurrencyMetaManager */
    private $currencyMetaManager;

    /** @var CurrencyRateManager */
    private $currencyManager;

    /** @var CurrencyRateCriteriasRepository */
    private $currencyRateCriteriasRepository;

    /** @var CurrencyCriteriasRepository */
    private $currencyCriteriasRepository;

    /** @var CurrencyRepository */
    private $currencyRepository;

    /**
     * CurrencyController constructor.
     *
     * @param CurrencyMetaRepository          $currencyMetaRepository
     * @param CurrencyMetaFactory             $currencyMetaFactory
     * @param CurrencyRateRepository          $currencyRateRepository
     * @param CbrManager                      $cbrManager
     * @param CurrencyMetaManager             $currencyMetaManager
     * @param CurrencyRateManager             $currencyManager
     * @param CurrencyRateCriteriasRepository $currencyRateCriteriasRepository
     * @param CurrencyCriteriasRepository     $currencyCriteriasRepository
     * @param CurrencyRepository              $currencyRepository
     */
    public function __construct(
        CurrencyMetaRepository $currencyMetaRepository,
        CurrencyMetaFactory $currencyMetaFactory,
        CurrencyRateRepository $currencyRateRepository,
        CbrManager $cbrManager,
        CurrencyMetaManager $currencyMetaManager,
        CurrencyRateManager $currencyManager,
        CurrencyRateCriteriasRepository $currencyRateCriteriasRepository,
        CurrencyCriteriasRepository $currencyCriteriasRepository,
        CurrencyRepository $currencyRepository
    ) {
        $this->currencyMetaRepository          = $currencyMetaRepository;
        $this->currencyMetaFactory             = $currencyMetaFactory;
        $this->currencyRateRepository          = $currencyRateRepository;
        $this->cbrManager                      = $cbrManager;
        $this->currencyMetaManager             = $currencyMetaManager;
        $this->currencyManager                 = $currencyManager;
        $this->currencyRateCriteriasRepository = $currencyRateCriteriasRepository;
        $this->currencyCriteriasRepository     = $currencyCriteriasRepository;
        $this->currencyRepository              = $currencyRepository;
    }

    /**
     * Главная страница
     *
     * @return LengthAwarePaginator|Factory|Response|View
     * @throws Exception
     */
    public function index()
    {
        /** @var Builder $query */
        $query = CurrencyRate::query();

        if (($sort = request('sort'))
            && in_array($sort,
                ['id', 'valute_id', 'country_number_code', 'country_string_code', 'nominal', 'name', 'value', 'date'],
                false)) {
            $query->orderBy($sort);
        }

        if ($filterName = request('filter_name')) {
            $query = $this->currencyRateCriteriasRepository->joinCurrencyCriteria($query);
            $query->where('name', 'LIKE', "%$filterName%");
            session(['name' => $filterName]);
        }

        if ($date = request('date_range')) {
            request()->validate([
                'date_range' => 'required|string',
            ]);

            session()->put('date_range', $date);

            [$from, $to] = explode(' - ', $date);

            $dto = new DateDTO();
            $dto->setTo(DateHelper::standardize($to));
            $dto->setFrom(DateHelper::standardize($from));
            $this->currencyRateCriteriasRepository->betweenDateCriteria($query, $dto);
        }

        $query = $query->paginate(20);

        return view('new_table', ['data' => $query]);
    }

    /**
     * @param $id
     *
     * @return Factory|View
     * @throws Exception
     */
    public function getOne($id)
    {
        /** @var Builder $query */
        $query = CurrencyRate::query();

        if (($sort = request('sort'))
            && in_array($sort,
                ['id', 'valute_id', 'country_number_code', 'country_string_code', 'nominal', 'name', 'value', 'date'],
                false)) {
            $query->orderBy($sort);
        }

        $currency = $this->currencyRepository->findOneByValuteId($id);

        if (!$currency) {
            throw new Exception();
        }

        $this->currencyRateCriteriasRepository->joinCurrencyCriteria($query);
        $this->currencyCriteriasRepository->findByValuteIdCriteria($query, $id);

        if ($date = request('date_range')) {
            request()->validate([
                'date_range' => 'required|string',
            ]);

            session()->put('date_range', $date);

            [$from, $to] = explode(' - ', $date);

            $dto = new DateDTO();
            $dto->setTo(DateHelper::standardize($to));
            $dto->setFrom(DateHelper::standardize($from));
            $this->currencyRateCriteriasRepository->betweenDateCriteria($query, $dto);
        }

        $query = $query->paginate(20);


        return view('one', ['data' => $query, 'id' => $currency->valute_id, 'name' => $currency->name]);
    }

    /**
     * Выгрузить данные за текущую дату
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function getLastData(): JsonResponse
    {
        $date = new DateTime();
        $meta = $this->currencyMetaRepository->findOneByDate(DateHelper::reformat($date));

        if (!$meta) {
            $meta = $this->currencyMetaFactory->handle($date);
        } else {
            if ($meta->status === CurrencyMeta::STATUS_COMPLETED) {
                return response()->json(['status' => 'Данные за текущую дату уже загружены в систему']);
            }

            if ($meta->status === CurrencyMeta::STATUS_IN_PROGRESS) {
                $meta->status = CurrencyMeta::STATUS_NEW;
                $this->currencyMetaRepository->save($meta);
                $this->currencyRateRepository->deleteAllByDate(DateHelper::reformat($date));
            }
        }

        $url = $this->cbrManager->getUrl(DateHelper::reformat($date, 'd/m/Y'));
        $xml = XmlHelper::getXMLByURL($url);

        if (trim((string)$xml) === 'Error in parameters') {
            $this->currencyMetaManager->metaComplete($meta);

            return response()->json(['status' => 'Ошибка в данных Банка за текущую дату']);
        }

        $this->currencyMetaManager->metaInProgress($meta);

        $dateXml = null;
        if (isset($xml['Date'])) {
            $dateXml = (new DateTime((string)$xml['Date']));
        } else {
            $dateXml = $date;
        }

        $metaExist = $this->currencyMetaRepository->findOneByDate(DateHelper::reformat($dateXml));
        if ($metaExist && $metaExist->status === CurrencyMeta::STATUS_COMPLETED) {
            $meta->status = CurrencyMeta::STATUS_COMPLETED;
            $this->currencyMetaRepository->save($meta);

            return response()->json(['status' => 'Данные за текущую дату уже загружены в систему']);
        }

        foreach ($xml->children() as $child) {
            $this->currencyManager->buildCurrency($child, $date);
        }

        $this->currencyMetaManager->metaComplete($meta);

        return response()->json(['status' => 'Обновлены данные за текущую дату']);
    }

    /**
     * Возвращает в JSON
     *
     * @return StreamedResponse
     * @throws Exception
     */
    public function getReport(): StreamedResponse
    {
        $query = CurrencyRate::query();
        $query = $this->currencyRateCriteriasRepository->selectReportCriteria($query);

        if ($date = request('date_range')) {
            request()->validate([
                'date_range' => 'required|string',
            ]);

            [$from, $to] = explode(' - ', $date);

            $dto = new DateDTO();
            $dto->setTo(DateHelper::standardize($to));
            $dto->setFrom(DateHelper::standardize($from));
            $this->currencyRateCriteriasRepository->betweenDateCriteria($query, $dto);

        } else {
            $now = DateHelper::standardize();
            $this->currencyRateCriteriasRepository->findByDateCriteria($query, $now);
        }

        if ($id = request('id')) {
            request()->validate([
                'id' => 'string',
            ]);

            $this->currencyCriteriasRepository->findByValuteIdCriteria($query, $id);
        }

        $this->currencyRateCriteriasRepository->joinCurrencyCriteria($query);

        $models = $query->get()
            ->toJson(JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK | JSON_PRETTY_PRINT);

        return response()->streamDownload(static function () use ($models) {
            echo $models;
        }, 'report.json');
    }

    /**
     * @return JsonResponse
     * @throws Exception
     */
    public function getGraphOne()
    {
        request()->validate([
            'date_range' => 'required|string',
            'id'         => 'required|string',
        ]);

        $date = request('date_range');
        $id   = request('id');

        [$from, $to] = explode(' - ', $date);

        $from = DateHelper::standardize($from);
        $to   = DateHelper::standardize($to);

        $dateDTO = new DateDTO();
        $dateDTO->setFrom($from);
        $dateDTO->setTo($to);

        $dto = new CurrencyGetGraphOneDTO();
        $dto->setValute($id);
        $dto->setDateDTO($dateDTO);


        $json = ['data' => $this->currencyManager->getGraphData($dto)];

        return response()->json($json);
    }
}
